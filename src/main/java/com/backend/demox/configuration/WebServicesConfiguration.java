package com.backend.demox.configuration;

import com.backend.demox.controllers.ws.ElementWsService;
import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;

import org.apache.cxf.transport.servlet.CXFServlet;
import org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor;
import org.apache.wss4j.dom.WSConstants;
import org.apache.wss4j.dom.handler.WSHandlerConstants;
import org.apache.wss4j.dom.validate.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.namespace.QName;
import javax.xml.ws.Endpoint;
import javax.xml.ws.soap.SOAPBinding;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class WebServicesConfiguration {

    /*
    @Bean
    public WSS4JInInterceptor wss4jInInterceptor(Validator credentialsValidator) {
        Map<String, Object> args = new HashMap<>(2);
        args.put(WSHandlerConstants.ACTION, WSHandlerConstants.USERNAME_TOKEN);
        args.put(WSHandlerConstants.PASSWORD_TYPE, WSConstants.PW_TEXT);

        Map<QName, Object> validators = new HashMap<>();
        QName qName = new QName(
                "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd",
                WSHandlerConstants.USERNAME_TOKEN, "");
        validators.put(qName, credentialsValidator);
        args.put(WSS4JInInterceptor.VALIDATOR_MAP, validators);

        return new WSS4JInInterceptor(args);
    }

*/

    @Bean
    public ServletRegistrationBean<CXFServlet> cxfServlet() {
        return new ServletRegistrationBean<>(new CXFServlet(), "/elements/ws/*");
    }

    @Bean(initMethod = "publish", destroyMethod = "stop")
    public Endpoint authenticationServiceEndpoint(Bus cxfBus,
                                                  ElementWsService elementWsService) {

        // EndpointImpl endpoint = new EndpointImpl(cxfBus, elementWsService, SOAPBinding.SOAP12HTTP_MTOM_BINDING);
        EndpointImpl endpoint = new EndpointImpl(cxfBus, elementWsService, SOAPBinding.SOAP12HTTP_BINDING);
        endpoint.setAddress("/elementWsService");

        //endpoint.getFeatures().add(new MtomFeature());
        endpoint.setPublishedEndpointUrl("/elements/ws/elementWsService");
        return endpoint;
    }

}
