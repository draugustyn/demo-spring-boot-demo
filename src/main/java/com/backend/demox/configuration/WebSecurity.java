package com.backend.demox.configuration;

import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import org.springframework.security.web.util.matcher.RequestMatcher;
import javax.servlet.http.HttpServletRequest;
import static org.springframework.security.config.Customizer.withDefaults;

    @Configuration
    @EnableWebSecurity
    @EnableGlobalMethodSecurity(prePostEnabled = true)

    public class WebSecurity{
        @Bean
        public SecurityFilterChain configure(HttpSecurity http) throws Exception {

            http
                    .cors().and()
                    .csrf().disable().authorizeHttpRequests()
                    .requestMatchers(new AntPathRequestMatcher("/clients/**")).hasRole("manager2")
                    .anyRequest().authenticated()
                    .and()
                    .formLogin();
            return http.build();
        }
        }



